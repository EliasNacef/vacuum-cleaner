
#include <set>
#include <vector>
#include "common.h"
#include "environment.h"
#include "tree.h"
#include "action_vacuum.h"
#include "state_vacuum.h"
#include "problem.h"
#include "vacuum.h"
#include "solution.h"

int main(int argc, char* argv[])
{
    Environment env = Environment(5, 5);
    env.UpdateEnvironment();

    Vacuum vac;
    Solution sol = Solution("bfs"); // default search mode with bfs
    if(argc == 2) sol = Solution(argv[1]);

	std::thread envThread([&]()
	{
        // ENVIRONMENT IS ON
        env.Run();
	});

    std::thread dispThread([&]()
    {
        // VACUUM IS ON
        sol.VacuumIsOn(env, vac);
    });

    std::cin.ignore();
    return EXIT_SUCCESS;
}