#pragma once

#include "point.h"
#include "common.h"
#include "Room.h"

/// <summary>
/// Environment to clean with a Vacuum
/// </summary>
class Environment
{
public:
	Environment();

	Environment(int weight, int height);

	Environment(const Environment& env) = default;

	~Environment() = default;;

	const std::vector<std::vector<Room>>& GetMap() const;

	void Disp();

	void Run();
	void UpdateEnvironment();

	int GetWidth() const;
	int GetHeight() const;

	void VacuumCleaning(Point position);
	void VacuumPicking(Point position);

	void UpdatePerformance(int x, int y);

private:
	int m_width;
	int m_height;

	std::vector<std::vector<Room>> m_map;
	int m_performance;

	void CreateMap();
};

