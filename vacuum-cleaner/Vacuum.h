#pragma once
#include "sensors.h"
#include "effectors.h"
#include "common.h"
#include "point.h"

/// <summary>
/// Vacuum agent class
/// </summary>
class Vacuum
{
public:
	/// <summary>
	/// ctor
	/// </summary>
	Vacuum();

	/// <summary>
	/// ctor
	/// </summary>
	Vacuum(int maxBattery);

	/// <summary>
	/// default ctor
	/// </summary>
	~Vacuum() = default;

	/// <summary>
	/// Give the vacuum's position
	/// </summary>
	/// <returns> Point m_point : position of the vacuum </returns>
	Point GetVacuumPosition();

	/// <summary>
	/// Return the sensors of the vacuum
	/// </summary>
	/// <returns> Sensors m_sensors : sensors of the vacuum </returns>
	Sensors GetSensors();

	/// <summary>
	/// Give the vacuum's effectors
	/// </summary>
	/// <returns> Effectors m_effectors : effectors of the vacuum </returns>
	Effectors GetEffectors();

	/// <summary>
	/// Call the method in the sensors to watch the environnment
	/// </summary>
	/// <param name="env"> environment we want to watch </param>
	void SensorsObserve(Environment env);

	/// <summary>
	/// Call the method in the effectors to do the actions
	/// </summary>
	/// <param name="actions"> set of actions we want to execute </param>
	/// <param name="environment"> environment where the vacuum will execute the actions </param>
	void EffectorsDo(vector<ActionVacuum> actions, Environment& environment);

	/// <summary>
	/// Call the method in the effectors to do one action
	/// </summary>
	/// <param name="actions"> the action we want to execute </param>
	/// <param name="environment"> environment where the vacuum will execute the action </param>
	void EffectorsDo(ActionVacuum action, Environment& environment);

	/// <summary>
	/// Disp observation known by agent
	/// </summary>
	void DispObservations() const;


private:
	Point m_vacuumPosition;

	Sensors m_sensors;
	Effectors m_effectors;
};




