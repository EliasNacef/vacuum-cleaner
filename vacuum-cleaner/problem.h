#pragma once

#include <set>
#include <map>
#include "node.h"
#include "data.h"

/// <summary>
/// Defines the problem to solve with AI
/// </summary>
template<typename S, typename A>
class Problem
{
public:

	Problem(S iniState, A noneAction, std::set<A> operators) : m_initialState(iniState), m_noneAction(noneAction), m_operators(operators)
	{

	}

	/// <summary>
	/// Return an action/state map according to every action/consequence from the state in parameter
	/// </summary>
	/// <param name="a"></param>
	/// <returns> The set of action/state </returns>
	virtual std::map<A, S> SuccessorFN(S state);

	/// <summary>
	/// Is the problem solved with the state parameter ?
	/// </summary>
	/// <param name="state"> Solution State </param>
	/// <returns> Is state a solution for this problem ? </returns>
	virtual bool GoalTest(S state);

	/// <summary>
	/// Return the cost of an action being in intialState
	/// </summary>
	/// <param name="initialState"> Initial state </param>
	/// <param name="action"> Action </param>
	/// <returns> The total cost </returns>
	virtual int StepCost(const S initialState, const A action) const;

	/// <summary>
	/// Getter for the initial state
	/// </summary>
	/// <returns></returns>
	virtual S GetInitialState();

	/// <summary>
	/// Getter for the initial action
	/// </summary>
	/// <returns></returns>
	virtual A GetInitialAction();

	/// <summary>
	/// Return the heristic for the node in parameter
	/// </summary>
	/// <returns></returns>
	virtual int GetHeuristic(const Data<S, A> data) const;

protected:
	S m_initialState;
	A m_noneAction;
	std::set<A> m_operators;
};

