#pragma once
#include<vector>
#include "common.h"
#include"environment.h"
#include "vacuum.h"
#include "vacuum_goal.h"
#include "state_vacuum.h"
#include "problem.h"
#include "tree.h"
class Solution
{
public:
	/// <summary>
	/// ctor
	/// </summary>
	Solution(string treeSearchMode) : m_treeSearchMode(treeSearchMode)
	{}

	/// <summary>
	/// Explore the environment and look for a solution with a tree search
	/// </summary>
	/// <param name="vacuum"> the vacuum which do the exploration </param>
	/// <returns> vector<ActionVacuum> actions : set of actions to go to a room to clean </returns>
	vector<ActionVacuum> ExploreEnvironmentWithTreeSearch(Vacuum& vacuum);

	/// <summary>
	/// All the action that the vacuum do to clean the environment
	/// </summary>
	/// <param name="environment"> the environment to clean </param>
	/// <param name="vacuum"> the vacuum which clean the environment </param>
	void VacuumIsOn(Environment& environment, Vacuum& vacuum);

private:
	string m_treeSearchMode;

	/// <summary>
	/// Transform a value of the enum action in a string
	/// </summary>
	/// <param name="type"> the value to transform </param>
	string ActionString(ActionVacuum type);
};

