#include "effectors.h"

/// <summary>
/// ctor
/// </summary>
Effectors::Effectors(): m_height(0), m_width(0){}

/// <summary>
/// Set m_height (height of the environment)
/// </summary>
/// <param name="height"> height value to set </param>
void Effectors::SetHeight(int height)
{
	m_height = height;
}

/// <summary>
/// Set m_width (width of the environment)
/// </summary>
/// <param name="width"> width value to set </param>
void Effectors::SetWidth(int width)
{
	m_width = width;
}

/// <summary>
/// Adjust the x value to go to the left
/// </summary>
/// <param name="position"> position to change</param>
void Effectors::GoingLeft(Point& position)
{
	int xPosition = position.GetX();
	if (xPosition > 0)
	{
		xPosition--;
		position.SetX(xPosition);
	}
}
 
/// <summary>
/// Adjust the x value to go to the right
/// </summary>
/// <param name="position"> position to change</param>
void Effectors::GoingRight(Point& position)
{
	int xPosition = position.GetX();
	if (xPosition < m_width - 1)
	{
		xPosition++;
		position.SetX(xPosition);
	}
}

/// <summary>
/// Adjust the y value to go up
/// </summary>
/// <param name="position"> position to change</param>
void Effectors::GoingUp(Point& position)
{
	int yPosition = position.GetY();
	if (yPosition > 0)
	{
		yPosition--;
		position.SetY(yPosition);
	}
}

/// <summary>
/// Adjust the y value to go down
/// </summary>
/// <param name="position"> position to change</param>
void Effectors::GoingDown(Point& position)
{
	int yPosition = position.GetY();
	if (yPosition < m_height - 1)
	{
		yPosition++;
		position.SetY(yPosition);
	}
}

/// <summary>
/// Simulate the cleaning of the room
/// </summary>
/// <param name="position"> position where the vacuum is</param>
/// <param name="environment"> environment to clean</param>
void Effectors::CleanRoom(Point position, Environment& environment)
{
	environment.VacuumCleaning(position);

	std::cout << "CLEAN ROOM" << std::endl;
}

/// <summary>
/// Simulate the picking of a jawel
/// </summary>
/// <param name="position"> position where the vacuum is</param>
/// <param name="environment"> environment where the jewel is</param>
void Effectors::PickUpJewel(Point position, Environment& environment)
{
	environment.VacuumPicking(position);

	std::cout << "PICK UP JEWEL" << std::endl;
}

/// <summary>
/// Do the set of actions to go to the room to clean and clean the room
/// </summary>
/// <param name="actions"> set of actions to do </param>
/// <param name="actions"> position of the vacuum </param>
/// <param name="environment"> environment to clean</param>
void Effectors::DoActions(vector<ActionVacuum> actions, Point& position, Environment& environment)
{
	// First do every action
	for (int actionIndex = 0; actionIndex < actions.size(); actionIndex++)
	{
		DoAction(actions[actionIndex], position);
	}
				
	// Get the jewel if there is one on the ground
	if (environment.GetMap()[position.GetX()][position.GetY()].GetHasJewel())
	{
		PickUpJewel(position, environment);
	}
	// Clean the dust if there is some
	if (environment.GetMap()[position.GetX()][position.GetY()].GetHasDust())
	{
		CleanRoom(position, environment);
	}
}

/// <summary>
/// Do one of the actions to go to the room
/// </summary>
/// <param name="actions"> action to do </param>
/// <param name="actions"> position of the vacuum </param>
void Effectors::DoAction(ActionVacuum action, Point& position)
{
	switch (action)
	{
	case ActionVacuum::RIGHT:
		GoingRight(position);
		break;
	case ActionVacuum::LEFT:
		GoingLeft(position);
		break;
	case ActionVacuum::UP:
		GoingUp(position);
		break;
	case ActionVacuum::DOWN:
		GoingDown(position);
		break;
	default:
		break;
	}
}
