#pragma once

#include "state.h"
#include "action_vacuum.h"
#include "environment.h"
#include "Vacuum.h"

/// <summary>
/// Derived class which describe a vacuum state and the current environment
/// </summary>
class StateVacuum : public State
{
public:
	StateVacuum(Environment environment, Vacuum v) : m_environment(environment), m_vacuum(v)
	{

	}

	Vacuum GetVacuum() const;
	Environment GetEnvironment() const;

	StateVacuum Estimate(ActionVacuum a); 

private:
	Vacuum m_vacuum;
	Environment m_environment;
};