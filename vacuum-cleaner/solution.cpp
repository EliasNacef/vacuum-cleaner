#include "solution.h"

/// <summary>
/// Explore the environment and look for a solution with a tree search
/// </summary>
/// <param name="vacuum"> the vacuum which do the exploration </param>
/// <returns> vector<ActionVacuum> actions : set of actions to go to a room to clean </returns>
vector<ActionVacuum> Solution::ExploreEnvironmentWithTreeSearch(Vacuum& vacuum)
{
	Environment env = vacuum.GetSensors().GetEnvironment();
	std::set<ActionVacuum> operators = { ActionVacuum::RIGHT, ActionVacuum::LEFT, ActionVacuum::UP, ActionVacuum::DOWN };
	StateVacuum initialState(env, vacuum);

	Problem<StateVacuum, ActionVacuum> problem(initialState, ActionVacuum::NONE, operators);

	Tree<StateVacuum, ActionVacuum> tree(problem);
	Node<Data<StateVacuum, ActionVacuum>>* solution;
	if(m_treeSearchMode == "greedy") solution = tree.GreedyTreeSearch(500);
	else if(m_treeSearchMode == "bfs") solution = tree.BFSTreeSearch(500);
	else
	{
		m_treeSearchMode = "bfs";
		solution = tree.BFSTreeSearch(500); // Default search
	}
	std::cout << m_treeSearchMode + " search mode :" << std::endl;
	std::cout << std::endl;
	std::cout << "Planning :" << std::endl;
	vector<ActionVacuum> actions;
	if (solution != nullptr)
	{
		actions.push_back(solution->data.action);
		while (solution->parent != nullptr)
		{
			solution = solution->parent;
			actions.push_back(solution->data.action);
		}
		std::vector<ActionVacuum>::iterator it;
		for (it = actions.begin() ; it != actions.end(); ++it)
		{
			std::cout << ActionString(*it) << std::endl;
		}
		return actions;
	}
	else
	{
		std::cout << "NO SOLUTION FOUND" << std::endl;
		return actions;
	}
}

/// <summary>
/// All the action that the vacuum do to clean the environment
/// </summary>
/// <param name="environment"> the environment to clean </param>
/// <param name="vacuum"> the vacuum which clean the environment </param>
void Solution::VacuumIsOn(Environment& environment, Vacuum& vacuum)
{
	auto start = std::chrono::high_resolution_clock::now();
	auto end = start;
	std::chrono::microseconds chrono = 0ms;
	while (true)
	{
		end = std::chrono::high_resolution_clock::now();
		chrono = std::chrono::duration_cast<std::chrono::microseconds>(end - start);
		// Every second, do :
		if (chrono >= 1s)
		{
			vacuum.SensorsObserve(environment);
			system("CLS"); // bad thing but good enough for this project
			vacuum.DispObservations();

			//vacuum.SetGoal();
			vector<ActionVacuum> actions = ExploreEnvironmentWithTreeSearch(vacuum);
			vacuum.EffectorsDo(actions, environment);
			start = std::chrono::high_resolution_clock::now();
		}
	}
}

/// <summary>
/// Transform a value of the enum action in a string
/// </summary>
/// <param name="type"> the value to transform </param>
string Solution::ActionString(ActionVacuum type) {
	switch (type) {
	case ActionVacuum::NONE:
		return "NONE";
	case ActionVacuum::RIGHT:
		return "RIGHT";
	case ActionVacuum::LEFT:
		return "LEFT";
	case ActionVacuum::UP:
		return "UP";
	case ActionVacuum::DOWN:
		return "DOWN";
	default:
		return "INVALID";
	}
}
