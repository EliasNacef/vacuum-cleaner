#pragma once

#include "common.h"

/// <summary>
/// Room to clean with a vacuum
/// </summary>
class Room
{
public:
	Room();

	~Room() = default;

	Room(const Room& room) = default;


	void AddDust();
	void AddJewel();
	bool GetHasDust() const;
	bool GetHasJewel() const;
	bool GetWasClean() const;
	bool GetWasCleanWithJewel() const;
	bool GetJewelPickedUp() const;
	bool GetPickedNothing() const;

	void ResetAllToFalse();

	std::string ToString() const;

	void VacuumIsCleaning();
	void VacuumIsPickingJewelUp();

	void Update();

private:

	bool m_wasClean;
	bool m_wasCleanWithJewel;
	bool m_jewelPickedUp;
	bool m_pickedNothing;
	bool m_hasDust;
	bool m_hasJewel;
};

