#pragma once
#include "common.h"
#include "point.h"
#include "environment.h"
#include "action_vacuum.h"
#include<vector>

/// <summary>
/// What could the vacuum do
/// </summary>
class Effectors
{
public:

	/// <summary>
	/// ctor
	/// </summary>
	Effectors();

	/// <summary>
	/// Set m_height (height of the environment)
	/// </summary>
	/// <param name="height"> height value to set </param>
	void SetHeight(int height);

	/// <summary>
	/// Set m_width (width of the environment)
	/// </summary>
	/// <param name="width"> width value to set </param>
	void SetWidth(int width);

	/// <summary>
	/// Adjust the x value to go to the left
	/// </summary>
	/// <param name="position"> position to change</param>
	void GoingLeft(Point& position);

	/// <summary>
	/// Adjust the x value to go to the right
	/// </summary>
	/// <param name="position"> position to change</param>
	void GoingRight(Point& position);

	/// <summary>
	/// Adjust the y value to go up
	/// </summary>
	/// <param name="position"> position to change</param>
	void GoingUp(Point& position);

	/// <summary>
	/// Adjust the y value to go down
	/// </summary>
	/// <param name="position"> position to change</param>
	void GoingDown(Point& position);

	/// <summary>
	/// Simulate the cleaning of the room
	/// </summary>
	/// <param name="position"> position where the vacuum is</param>
	/// <param name="environment"> environment to clean</param>
	void CleanRoom(Point position, Environment& environment);

	/// <summary>
	/// Simulate the picking of a jawel
	/// </summary>
	/// <param name="position"> position where the vacuum is</param>
	/// <param name="environment"> environment where the jewel is</param>
	void PickUpJewel(Point position, Environment& environment);

	/// <summary>
	/// Do the set of actions to go to the room to clean and clean the room
	/// </summary>
	/// <param name="actions"> set of actions to do </param>
	/// <param name="actions"> position of the vacuum </param>
	/// <param name="environment"> environment to clean</param>
	void DoActions(vector<ActionVacuum> actions, Point& position, Environment& environment);

	/// <summary>
	/// Do one of the actions to go to the room
	/// </summary>
	/// <param name="actions"> action to do </param>
	/// <param name="actions"> position of the vacuum </param>
	void DoAction(ActionVacuum action, Point& position);

private:
	int m_height;
	int m_width;
};

