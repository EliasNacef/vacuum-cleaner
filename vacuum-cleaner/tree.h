#pragma once

#include <map>
#include <vector>
#include <list>
#include <queue>

#include "node.h"
#include "problem.h"
#include "state.h"
#include "data.h"

#define MAX_TRIALS 100000

/// <summary>
/// Tree seach class
/// </summary>
/// <typeparam name="A"> Action </typeparam>
/// <typeparam name="S"> State </typeparam>
template<typename S, typename A>
class Tree
{
public:
	/// <summary>
	/// ctor
	/// </summary>
	Tree(Problem<S, A> pb) : m_problem(pb)
	{
		S state = m_problem.GetInitialState();
		A action = m_problem.GetInitialAction();
		Data<S, A> data(state, action);
		m_root = new Node<Data<S, A>>(data);
	}

	/// <summary>
	/// dtor
	/// </summary>    
	~Tree()
	{
		for (auto& node : m_nodes)
		{
			delete node;
		}
	}

	/// <summary>
	/// Root getter
	/// </summary>
	/// <typeparam name="A"> Action type </typeparam>
	/// <typeparam name="S"> State type </typeparam>
	Node<Data<S, A>>* GetRoot()
	{
		return m_root;
	}


	/// <summary>
	/// Define every attribute of the current node and create new ones thanks to succession function
	/// </summary>
	/// <param name="node"> Initial problem node </param>
	/// <param name="problem"> Problem to solve </param>
	/// <returns> Set of new nodes </returns>
	std::vector<Node<Data<S, A>>*> Expand(Node<Data<S, A>>* node, Problem<S, A> problem)
	{
		std::vector<Node<Data<S, A>>*> successors;
		if (node == nullptr) return successors;
		std::map<A, S> resultsMap = problem.SuccessorFN(node->data.state);
		for (auto it : resultsMap)
		{
			Data<S, A> data(it.second, it.first);
			Node<Data<S, A>>* s = new Node<Data<S, A>>(data);
			m_nodes.push_back(s);
			s->parent = node;
			s->cost = node->cost + problem.GetHeuristic(node->data);
			s->depth = node->depth + 1;
			successors.push_back(s);
		}
		return successors;
	}

	/// <summary>
	/// Handle the tree seach to find a solution to the tree problem
	/// </summary>
	/// <returns> The solution if it exists </returns>
	Node<Data<S, A>>* BFSTreeSearch(int searchLimit)
	{
		if (searchLimit < 0) searchLimit = MAX_TRIALS;
		int trial = 0;
		m_currentFringe.push(m_root);
		while (!m_currentFringe.empty() && searchLimit > trial++)
		{
			Node<Data<S, A>>* node = m_currentFringe.front();
			m_currentFringe.pop();
			if (m_problem.GoalTest(node->data.state)) return node;
			for (const auto& node : Expand(node, m_problem))
			{
				m_currentFringe.push(node);
			}
		}
		return nullptr;
	}

	/// <summary>
	/// Handle the A* tree seach to find a solution to the tree problem
	/// </summary>
	/// <returns> The solution if it exists </returns>
	Node<Data<S, A>>* GreedyTreeSearch(int searchLimit)
	{
		int trial = 0;
		m_currentFringe.push(m_root);
		while (!m_currentFringe.empty() && searchLimit > trial++)
		{
			Reorder(m_currentFringe); // Reorder according to the heuristic (cost)
			Node<Data<S, A>>* node = m_currentFringe.front();
			m_currentFringe.pop();
			if (m_problem.GoalTest(node->data.state))
			{
				return node;
			}
			for (const auto& node : Expand(node, m_problem))
			{
				m_currentFringe.push(node);
			}
		}
		return nullptr;
	}


private:
	Problem<S, A> m_problem;
	std::queue<Node<Data<S, A>>*> m_currentFringe;
	std::vector<Node<Data<S, A>>*> m_nodes;
	Node<Data<S, A>>* m_root;

	void Reorder(std::queue<Node<Data<S, A>>*>& fringe)
	{
		std::vector<Node<Data<S, A>>*> vFringe;
		while (!fringe.empty())
		{
			vFringe.push_back(fringe.front());
			fringe.pop();
		}

		std::sort(vFringe.begin(), vFringe.end(), [this](const Node<Data<S, A>>* a, const Node<Data<S, A>>* b)
		{
			if (a != nullptr)
			{
				if (b != nullptr)
				{
					return (a->cost < b->cost);
				}
				return true;
			}
			return false;
		});

		for (int i = 0; i < vFringe.size(); i++)
		{
			fringe.push(vFringe[i]);
		}
	}
};