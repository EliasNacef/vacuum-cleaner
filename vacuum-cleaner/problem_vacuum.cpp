#include "problem.h"
#include "state_vacuum.h"
#include "action_vacuum.h"
#include "vacuum.h"
#include "environment.h"
#include "point.h"

std::map<ActionVacuum, StateVacuum> Problem<StateVacuum, ActionVacuum>::SuccessorFN(StateVacuum currentState)
{
	std::map<ActionVacuum, StateVacuum> map;
	// Generate neighbours states after simulating what could happen with an operator
	for (auto& op : m_operators)
	{
		Vacuum vac = Vacuum(currentState.GetVacuum());
		vac.SensorsObserve(currentState.GetEnvironment());
		Environment env = Environment(currentState.GetEnvironment());
		vac.EffectorsDo(op, env);
		StateVacuum state = StateVacuum(env, vac);
		std::pair<ActionVacuum, StateVacuum> pair(op, state);
		map.emplace(pair);
	}
	return map;
}


bool Problem<StateVacuum, ActionVacuum>::GoalTest(StateVacuum state)
{
	Point vacuumPosition = state.GetVacuum().GetVacuumPosition();
	Environment stateEnvironment = state.GetEnvironment();
	std::vector<std::vector<Room>> stateMap = stateEnvironment.GetMap();
	if (stateMap[vacuumPosition.GetX()][vacuumPosition.GetY()].GetHasDust() || stateMap[vacuumPosition.GetX()][vacuumPosition.GetY()].GetHasJewel()) return true;
	return false;
}


int Problem<StateVacuum, ActionVacuum>::StepCost(const StateVacuum currentState, const ActionVacuum action) const
{
	return 1;
}

StateVacuum Problem<StateVacuum, ActionVacuum>::GetInitialState()
{
	return m_initialState;
}

/// <summary>
/// Getter for the initial action
/// </summary>
/// <returns></returns>
ActionVacuum Problem<StateVacuum, ActionVacuum>::GetInitialAction()
{
	return m_noneAction;
}

/// <summary>
/// Heuristic choosen for the problem
/// </summary>
/// <param name="data"></param>
/// <returns></returns>
int Problem<StateVacuum, ActionVacuum>::GetHeuristic(Data<StateVacuum, ActionVacuum> data) const
{
	//return StepCost(data.state, data.action);
	StateVacuum currentState = data.state;
	Vacuum vacuum = currentState.GetVacuum();
	Point currentPos = vacuum.GetVacuumPosition();
	Environment env = currentState.GetEnvironment();
	Point center = Point(env.GetWidth()/2, env.GetHeight()/2);
	return Point::GridDistance(currentPos, center);
}