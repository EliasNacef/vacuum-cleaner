#include "graph.h"

Graph::Graph(int order) : m_edges(new std::list<int>[order]), m_order(order)
{
}

void Graph::AddEdge(int node, int other)
{
    m_edges.get()[node].push_back(other);
}

void Graph::BFS(int firstNode)
{
    std::vector<int> res;
    std::vector<bool> visited(m_order);
    std::fill(visited.begin(), visited.end(), false);

    std::list<int> queue;

    visited[firstNode] = true;
    queue.push_back(firstNode);

    std::list<int>::iterator it;
    while (!queue.empty())
    {
        firstNode = queue.front();
        res.push_back(firstNode);
        queue.pop_front();

        for (it = m_edges.get()[firstNode].begin(); it != m_edges.get()[firstNode].end(); ++it)
        {
            if (!visited[*it])
            {
                visited[*it] = true;
                queue.push_back(*it);
            }
        }
    }
    
    for (auto& value : res)
    {
        std::cout << value << std::endl;
    }
}