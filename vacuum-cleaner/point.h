#pragma once
#include <iostream>
#include<string>

using namespace std;

class Point
{
public:

    /*
     * Constructor: Point
     * Usage: Point origin;
     *        Point pt(x, y);
     * ----------------------
     * Creates a Point object with the specified x and y coordinates.  If the
     * coordinates are not supplied, the default constructor sets these fields
     * to 0.
     */

    Point();
    Point(int x, int y);

    /*
     * Method: getX
     * Usage: int x = pt.getX();
     * -------------------------
     * Returns the x-coordinate of the point.
     */

    int GetX() const;

    /*
     * Method: getY
     * Usage: int y = pt.getY();
     * -------------------------
     * Returns the y-coordinate of the point.
     */

    int GetY() const;

    /*
     * Method: setX
     * Usage: pt.setX(int x);
     * -------------------------
     * Set the x-coordinate of the point.
     */

    void SetX(int x);

    /*
     * Method: setY
     * Usage: pt.setY(int y);
     * -------------------------
     * Set the y-coordinate of the point.
     */
    void SetY(int y);

    /*Return grid distance */
    static int GridDistance(Point a, Point b);


private: 
    int m_x;
    int m_y;

};

