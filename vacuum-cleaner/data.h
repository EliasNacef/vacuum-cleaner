#pragma once

/// <summary>
/// Data inside a node
/// </summary>
/// <typeparam name="S"></typeparam>
/// <typeparam name="A"></typeparam>
template<typename S, typename A>
struct Data
{
    Data(S s, A a) : state(s), action(a)
    {

    }

    S state; // a state
    A action; // an action
};