#pragma once

enum class VacuumGoal
{
	RECHARGE, FIND_NON_EMPTY_ROOM
};