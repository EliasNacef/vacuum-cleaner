#include "room.h"

Room::Room(): m_hasDust(false), m_hasJewel(false),
				m_wasClean(false), m_wasCleanWithJewel(false), m_jewelPickedUp(false), m_pickedNothing(false)
{

}

bool Room::GetHasDust() const
{
	return m_hasDust;
}

bool Room::GetHasJewel() const
{
	return m_hasJewel;
}

bool Room::GetWasClean() const
{
	return m_wasClean;
}

bool Room::GetWasCleanWithJewel() const
{
	return m_wasCleanWithJewel;
}

bool Room::GetJewelPickedUp() const
{
	return m_jewelPickedUp;
}

bool Room::GetPickedNothing() const
{
	return m_pickedNothing;
}

void Room::ResetAllToFalse()
{
	m_wasClean = false;
	m_wasCleanWithJewel = false;
	m_jewelPickedUp = false;
	m_pickedNothing = false;
}

std::string Room::ToString() const
{
	std::string res = " ";
	if (m_hasDust)
	{
		res += "D";
	}
	else
	{
		res += " ";
	}

	if (m_hasJewel)
	{
		res += "J";
	}
	else
	{
		res += " ";
	}
	res += " ";
	return res;
}


void Room::AddDust()
{
	m_hasDust = true;
}

void Room::AddJewel()
{
	m_hasJewel = true;
}

void Room::Update()
{
	// Random Generation of Dust (1/50)
	if (rand() % 50 == 0)
	{
		AddDust();
	}
	// Random Generation of jewel
	if (rand() % 50 == 0)
	{
		AddJewel();
	}
}

void Room::VacuumIsCleaning()
{
	if (m_hasDust && !m_hasJewel)
	{
		m_wasClean = true;
	}

	if (m_hasDust && m_hasJewel) 
	{
		m_wasCleanWithJewel = true;
	}

	if (!m_hasDust && m_hasJewel)
	{
		m_wasCleanWithJewel = true;
	}

	m_hasDust = false;
	m_hasJewel = false;
}

void Room::VacuumIsPickingJewelUp()
{
	if (m_hasJewel)
	{
		m_jewelPickedUp = true;
	}

	else
	{
		m_pickedNothing = true;
	}
	
	m_hasJewel = false;
}