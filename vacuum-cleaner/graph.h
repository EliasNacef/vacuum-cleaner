#include<iostream>
#include <list>
#include <vector>


class Graph
{
public:
    Graph(int order);

    void AddEdge(int node, int other);

    void BFS(int firstNode);

private:
    int m_order;
    std::unique_ptr<std::list<int>> m_edges;
};