#include "Vacuum.h"

/// <summary>
/// ctor
/// </summary>
Vacuum::Vacuum() : m_sensors(Sensors()), m_effectors(Effectors()), m_vacuumPosition(Point(0, 0))
{

}


/// <summary>
/// ctor
/// </summary>
Vacuum::Vacuum(int maxBattery) :
	m_sensors(Sensors()), m_effectors(Effectors()),m_vacuumPosition(Point(0, 0))
{

}

/// <summary>
/// Give the vacuum's position
/// </summary>
/// <returns> Point m_point : position of the vacuum </returns>
Point Vacuum::GetVacuumPosition()
{
	return m_vacuumPosition;
}

/// <summary>
/// Return the sensors of the vacuum
/// </summary>
/// <returns> Sensors m_sensors : sensors of the vacuum </returns>
Sensors Vacuum::GetSensors()
{
	return m_sensors;
}

/// <summary>
/// Give the vacuum's effectors
/// </summary>
/// <returns> Effectors m_effectors : effectors of the vacuum </returns>
Effectors Vacuum::GetEffectors()
{
	return m_effectors;
}

/// <summary>
/// Call the method in the sensors to watch the environnment
/// </summary>
/// <param name="env"> environment we want to watch </param>
void Vacuum::SensorsObserve(Environment env)
{
	m_sensors.ObserveEnvironmentWithAllMySensors(env);
}

/// <summary>
/// Call the method in the effectors to do the actions
/// </summary>
/// <param name="actions"> set of actions we want to execute </param>
/// <param name="environment"> environment where the vacuum will execute the actions </param>
void Vacuum::EffectorsDo(vector<ActionVacuum> actions, Environment& environment)
{
	m_effectors.SetHeight(m_sensors.GetHeight());
	m_effectors.SetWidth(m_sensors.GetWidth());
	m_effectors.DoActions(actions, m_vacuumPosition, environment);
}

/// <summary>
/// Call the method in the effectors to do one action
/// </summary>
/// <param name="actions"> the action we want to execute </param>
/// <param name="environment"> environment where the vacuum will execute the action </param>
void Vacuum::EffectorsDo(ActionVacuum action, Environment& environment)
{
	m_effectors.SetHeight(m_sensors.GetHeight());
	m_effectors.SetWidth(m_sensors.GetWidth());
	m_effectors.DoAction(action, m_vacuumPosition);
}

/// <summary>
/// Disp observation known by agent
/// </summary>
void Vacuum::DispObservations() const
{
	std::string line = "";
	for (size_t j = 0; j < m_sensors.GetWidth(); j++)
		line += "-------";
	std::cout << line << std::endl;

	for (size_t j = 0; j < m_sensors.GetHeight(); j++)
	{
		for (size_t i = 0; i < m_sensors.GetWidth(); i++)
		{
			std::cout << "|";
			std::cout << m_sensors.GetEnvironment().GetMap()[i][j].ToString();
			if (m_vacuumPosition.GetX() == i && m_vacuumPosition.GetY() == j)
			{
				std::cout << "V ";
			}
			else
			{
				std::cout << "  ";
			}
		}
		std::cout << "|" << std::endl;
		std::cout << line << std::endl;
	}
}
