#pragma once
#include "common.h"
#include "environment.h"

/// <summary>
/// What could the vacuum observe
/// </summary>
class Sensors
{
public:
	/// <summary>
	/// ctor
	/// </summary>
	Sensors();

	/// <summary>
	/// Copy the environment in m_environment
	/// </summary>
	/// <param name="actions"> set of actions we want to execute </param>
	void ObserveEnvironmentWithAllMySensors(Environment environment);

	/// <summary>
	/// Return the environment see by the sensors
	/// </summary>
	/// <returns> Environment m_environment : environment saw by the sensors </returns>
	const Environment& GetEnvironment() const;

	/// <summary>
	/// Return the number of rooms in the environment see by the sensors
	/// </summary>
	/// <returns> int m_rooms: the number of rooms </returns>
	int GetRooms() const;

	/// <summary>
	/// Return the number of dirty rooms in the environment see by the sensors
	/// </summary>
	/// <returns> int m_rooms: the number of dirty rooms </returns>
	int GetDirtyRooms() const;

	/// <summary>
	/// Return the height the environment see by the sensors
	/// </summary>
	/// <returns> int m_height: the height of the environment </returns>
	int GetHeight() const;

	/// <summary>
	/// Return the width the environment see by the sensors
	/// </summary>
	/// <returns> int m_width: the width of the environment </returns>
	int GetWidth() const;

private:
	Environment m_environment;
	int m_rooms;
	int m_dirtyRooms;

	int m_height;
	int m_width;
};