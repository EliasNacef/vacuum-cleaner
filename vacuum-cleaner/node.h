#pragma once

#include <vector>
#include <map>

/// <summary>
/// Node structure : equivalent to a tree
/// </summary>
/// <typeparam name="T"></typeparam>
template<typename T>
struct Node {
    Node(T v)
        : data(v), cost(0), depth(0), parent(nullptr) {}

    T data;
    int cost;
    int depth;
    Node* parent;
};