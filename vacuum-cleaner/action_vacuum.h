#pragma once

/// <summary>
/// Describes vacuum actions availables
/// </summary>
enum class ActionVacuum
{
	NONE, RIGHT, LEFT, UP, DOWN
};