#include "environment.h"
#include <ctime>

/// <summary>
/// ctor
/// </summary>
Environment::Environment() : Environment(1, 1)
{

}

/// <summary>
/// ctor with a size defined
/// </summary>
/// <param name="width"> Environment Width </param>
/// <param name="height"> Environment Height </param>
Environment::Environment(int width, int height) : m_width(width), m_height(height), m_performance(0)
{
	CreateMap();
}

const std::vector<std::vector<Room>>& Environment::GetMap() const
{
	return m_map;
}

void Environment::CreateMap()
{
	for (size_t j = 0; j < m_height; j++)
	{
		std::vector<Room> row;
		for (size_t i = 0; i < m_width; i++)
		{
			Room r;
			row.push_back(r);
		}
		m_map.push_back(row);
	}
}


void Environment::Disp()
{
	std::string line = "";
	for (size_t j = 0; j < m_width; j++)
		line += "-----";
	std::cout << line << std::endl;

	for (size_t j = 0; j < m_height; j++)
	{
		for (size_t i = 0; i < m_width; i++)
		{
			std::cout << "|";
			std::cout << m_map[i][j].ToString();
		}
		std::cout << "|" << std::endl;
		std::cout << line << std::endl;
	}
	std::cout << m_performance << std::endl;
}

void Environment::Run()
{
	auto start = std::chrono::high_resolution_clock::now();
	auto end = start;
	std::chrono::microseconds chrono = 0ms;
	while (true)
	{
		end = std::chrono::high_resolution_clock::now();
		chrono = std::chrono::duration_cast<std::chrono::microseconds>(end - start);
		// Every second, do :
		if (chrono >= 1s)
		{
			UpdateEnvironment();
			start = std::chrono::high_resolution_clock::now();
		}
	}
}

void Environment::UpdateEnvironment()
{
	srand((int)time(0));
	for (size_t j = 0; j < m_height; j++)
	{
		for (size_t i = 0; i < m_width; i++)
		{
			UpdatePerformance(i, j);
			m_map[i][j].Update();
		}
	}
	//system("CLS"); // bad thing but good enough for this project
	//Disp();
}

int Environment::GetWidth() const
{
	return m_width;
}

int Environment::GetHeight() const
{
	return m_height;
}

void Environment::VacuumCleaning(Point position)
{
	int x = position.GetX();
	int y = position.GetY();
	m_map[x][y].VacuumIsCleaning();
}

void Environment::VacuumPicking(Point position)
{
	int x = position.GetX();
	int y = position.GetY();
	m_map[x][y].VacuumIsPickingJewelUp();
}

void Environment::UpdatePerformance(int x, int y) 
{
	bool wasClean = m_map[x][y].GetWasClean();
	bool wasCleanWithJewel = m_map[x][y].GetWasCleanWithJewel();
	bool jewelPickedUp = m_map[x][y].GetJewelPickedUp();
	bool pickedNothing = m_map[x][y].GetPickedNothing();
	if (wasClean || jewelPickedUp) 
	{
		m_performance++;
	}

	if (wasCleanWithJewel || pickedNothing) 
	{
		m_performance--;
	}

	m_map[x][y].ResetAllToFalse();
}