#include "sensors.h"

/// <summary>
/// ctor
/// </summary>
Sensors::Sensors() : m_environment(Environment()), m_rooms(0), m_dirtyRooms(0), m_height(0), m_width(0)
{

}

/// <summary>
/// Copy the environment in m_environment
/// </summary>
/// <param name="actions"> set of actions we want to execute </param>
void Sensors::ObserveEnvironmentWithAllMySensors(Environment environment)
{
	m_environment = environment;
	m_rooms = 0;
	m_dirtyRooms = 0;

	std::vector<std::vector<Room>> map = m_environment.GetMap();
	m_height = map.size();
	for (int i = 0; i < m_height; i++)
	{
		m_width = map[i].size();
		for (int j = 0; j < m_width; j++)
		{
			m_rooms++;

			if (map[i][j].GetHasDust())
			{
				m_dirtyRooms++;
			}
		}
	}
}

/// <summary>
/// Return the environment see by the sensors
/// </summary>
/// <returns> Environment m_environment : environment saw by the sensors </returns>
const Environment& Sensors::GetEnvironment() const
{
	return m_environment;
}

/// <summary>
/// Return the number of rooms in the environment see by the sensors
/// </summary>
/// <returns> int m_rooms: the number of rooms </returns>
int Sensors::GetRooms() const
{
	return m_rooms;
}

/// <summary>
/// Return the number of dirty rooms in the environment see by the sensors
/// </summary>
/// <returns> int m_rooms: the number of dirty rooms </returns>
int Sensors::GetDirtyRooms() const
{
	return m_dirtyRooms;
}

/// <summary>
/// Return the height the environment see by the sensors
/// </summary>
/// <returns> int m_height: the height of the environment </returns>
int Sensors::GetHeight() const
{
	return m_height;
}

/// <summary>
/// Return the width the environment see by the sensors
/// </summary>
/// <returns> int m_width: the width of the environment </returns>
int Sensors::GetWidth() const
{
	return m_width;
}